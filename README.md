# NFT's as Games / Content


## Idea

Can we allow app creators to sell their content as NFT's allowing them to keep all the profits of their work rather than going through a normal app store?

## Plan

1. Deploy an NFT (https://ethereum.org/en/developers/tutorials/how-to-write-and-deploy-an-nft/)
2. Create a Web 3 page that connects to metamask (how did we do this for leaderboard?)
3. Show content based on having a wallet
4. Show content based on the existence of a specific NFT


## Additional Features

1. Keep checking if the NFT is there to avoid bad actors
2. Find a way to make it more secure
3. Allow people to easily integrate with the platform (OpenSea + Shopify?)